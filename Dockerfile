
FROM python:3.7.5

ENV PYTHONUNBUFFERED 1

RUN mkdir /ayomi

WORKDIR /ayomi

ADD . /ayomi/

RUN pip install -r requirements.txt
