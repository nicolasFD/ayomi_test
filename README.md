 # Ayomi test backend

This is a python 3.7.5 app backend for development purpose (not production ready).
Based on the django framework, the django-allauth package and bootstrap for frontend.
It supports Sign up, login, logout on users.
It uses a custom user from the built-in user model of django.

# Docker

To build the container, run
`docker build . -t ayomi:latest`

To boot the system, run
`docker-compose up` or `docker-compose up -d`

The app should be available on localhost:8000
