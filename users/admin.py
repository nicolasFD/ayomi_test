from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

from .forms import AyomiUserCreationForm, AyomiUserChangeForm
from .models import AyomiUser

class AyomiUserAdmin(UserAdmin):
    add_form = AyomiUserCreationForm
    form = AyomiUserChangeForm
    model = AyomiUser
    list_display = ['email', 'username',]

admin.site.register(AyomiUser, AyomiUserAdmin)