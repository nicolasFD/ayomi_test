from django.shortcuts import render
from django.views.generic import TemplateView

class AccountPageView(TemplateView):
    template_name = 'account.html'
    def get(self, request):
        form = UpdateMailForm()
        return render(request, 'account.html', {'form_update': form})
