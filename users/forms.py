from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from allauth.account.forms import LoginForm
from core.validators.validators import validate_email
from .models import AyomiUser

class AyomiUserCreationForm(UserCreationForm):
    email = forms.EmailField(validators = [validate_email])
    class Meta:
        model = AyomiUser
        fields = ('username', 'email')


class AyomiUserChangeForm(UserChangeForm):
    email = forms.EmailField(validators = [validate_email])
    class Meta:
        model = AyomiUser
        fields = ('username', 'email')


class UpdateMailForm(forms.Form):
    email = forms.EmailField(validators=[validate_email])
