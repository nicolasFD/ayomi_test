from django.urls import path, include

from .views import AccountPageView

urlpatterns = [
    path('', AccountPageView.as_view(), name='account'),
]