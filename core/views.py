from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.views.generic import TemplateView
from core.validators import validators
from users.forms import UpdateMailForm
from users.models import AyomiUser


class HomePageView(TemplateView):
    template_name = 'home.html'

    def get(self, request):
        form = UpdateMailForm()
        return render(request, 'home.html', {'form_update': form})

    def post(self, request):
        if not request.user.is_authenticated:
            return render(request, 'home.html')

        user_id = request.user.id

        if request.is_ajax():
            email = request.POST.get('email', None)
            try:
                # check if input is a valid mail adress
                validate_email(email)
                # check email address is unique
                validators.validate_email(email)
                valid_email = True
            except ValidationError as e:
                message = e.message
                valid_email = False

            if valid_email:
                user = AyomiUser.objects.get(id=user_id)
                user.email = email
                user.save()
                message = "Adresse email modifiée"
                return JsonResponse({'success': valid_email,'user_email': user.email, 'message': message})
            else:
                user = AyomiUser.objects.get(id=user_id)
                return JsonResponse({'success': valid_email, 'user_email': user.email, 'message': message})

