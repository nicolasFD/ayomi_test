from django.core.exceptions import ValidationError
from users.models import AyomiUser

def validate_email(value):
    if AyomiUser.objects.filter(email=value).exists():
        raise ValidationError(
            (f"{value} est déjà utilisé."),
            params = {'value':value}
        )
