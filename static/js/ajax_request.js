let notification = function (data, color) {
    let message = $('#message_update')
    let body_background = $("body"), t = 100, originalColor = body_background.css("background");
    let text_color_class = data.success ? "text-success" : "text-danger"

    message.removeClass()
    message.text(data.message);
    message.addClass(text_color_class)

    $('.control').show().delay(2000)

    body_background.css("background", color);
    setTimeout(function () {
        body_background.css("background", originalColor);
    }, t);
}

$(document).ready(function () {
    $('#save_button').on('click', function (e) {
        e.preventDefault();
        let user_email = $('#id_email').val();

        $.ajax({
            type: "POST",
            url: "/",
            data: {
                email: $('#id_email').val(),
                csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
            },
            success: function (data) {
                let color = "#dc3545";
                if (data.success) {
                    color = "green";
                }

                $('#UpdateModal').modal('hide');
                $('#user_email').text(data.user_email);
                notification(data, color);
            },
            failure: function (data) {
                $('#UpdateModal').modal('hide')
            }
        });
    });
})